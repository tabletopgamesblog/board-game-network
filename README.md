To add the logos or text of the members of the Board Game Media Network to your site,
add the following <script> to wherever you want the list to appear:
  
Logo

```javascript
<script src="https://bbcdn.githack.com/tabletopgamesblog/board-game-network/raw/master/logo_network.js">
```

Text
```javascript
<script src="https://bbcdn.githack.com/tabletopgamesblog/board-game-network/raw/master/text_network.js">
```